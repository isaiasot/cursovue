﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Vue.Models.Clases;
using Vue.Models.Context;

namespace Vue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutenticacionController : ControllerBase
    {
        private readonly IConfiguration config;
        private readonly TODOContext bd;

        public AutenticacionController(IConfiguration _config, TODOContext _bd) {
            config = _config;
            bd = _bd;
        }

        public int? UbtenerUsuario(string usu, string pwd) {
            var cred = new TarjetaAcceso();
            config.GetSection("TarjetaAcceso").Bind(cred);
            if (string.IsNullOrWhiteSpace(cred.LlaveDeAcceso) || string.IsNullOrWhiteSpace(cred.QuienEres))
                return null;
            if (cred.QuienEres.Equals(usu) && cred.LlaveDeAcceso.Equals(pwd))
                return 1;
            else
                return 2;
        }

        [Route("Token"), HttpPost, AllowAnonymous]
        public IActionResult Autenticar([FromBody] Credencial cred) {
            if (!ModelState.IsValid)
                return StatusCode(400, ModelState);
            var id = UbtenerUsuario(cred.Usuario, cred.Password);
            if (!id.HasValue)
                return StatusCode(400, "Usuario o Contraseña inválido");
            if (id.Value == 2)
                return StatusCode(400, "Contraseña inválida");

            var token = new JwtSecurityToken(
                issuer: "hacsys.com",
                audience: "hacsys.com",
                claims: new List<Claim> {
                        // El Usuario se registro con su Email
                        new Claim(JwtRegisteredClaimNames.Email, cred.Usuario),
                        // El Token
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                },
                expires: DateTime.Now.AddMinutes(5),
                notBefore: DateTime.Now,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("System.ArgumentOutOfRangeException")),
                    SecurityAlgorithms.HmacSha256
                )
            );
            return StatusCode(200, new { token = new JwtSecurityTokenHandler().WriteToken(token) });

        }
    }
}
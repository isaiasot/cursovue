﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Vue.Models.Context;

namespace Vue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly TODOContext bd;

        public TodoController(TODOContext _bd) { bd = _bd; }

        [Route("GetTarea"), HttpGet, Authorize]
        public async Task<ActionResult> GetTarea(string tarea)
        {
            if (!ModelState.IsValid)
                return StatusCode(400, ModelState);
            var tareas = await bd.Todos.FromSql("Todo.BuscarTarea @Tarea", new SqlParameter("Tarea", tarea)).ToListAsync<Models.Clases.Todo>();
            if (tareas == null || tareas.Count < 1)
                return StatusCode(404, $"No se encontraron tareas para {tarea}");
            return StatusCode(200, tareas);
        }

        [Route("GetTareas"), HttpGet, Authorize]
        public async Task<IActionResult> GetTareas()
        {
            if (!ModelState.IsValid)
                return StatusCode(400, ModelState);
            var tareas = await bd.Todos.FromSql("Todo.GetTarea").ToListAsync<Models.Clases.Todo>();
            if (tareas == null || tareas.Count < 1)
                return StatusCode(404, $"No se encontraron tareas");
            return StatusCode(200, tareas);
        }

        [Route("AddTarea"), HttpPost, Authorize]
        public async Task<IActionResult> AddTarea([FromBody] Models.Clases.Todo todo)
        {
            if (!ModelState.IsValid)
                return StatusCode(400, ModelState);
            if (string.IsNullOrWhiteSpace(todo.Tarea))
                return StatusCode(400, "No se indico una tarea. Tareas en blanco no son válidas");
            await bd.Database.ExecuteSqlCommandAsync("Todo.AddTarea @Tarea, @Completada", new[] { new SqlParameter("Tarea", todo.Tarea), new SqlParameter("Completada", todo.Completada) });
            return StatusCode(200, "Insertado");
        }

        [Route("UpdTarea"), HttpPut, Authorize]
        public async Task<IActionResult> UpdTarea([FromBody] Models.Clases.Todo todo)
        {
            if (!ModelState.IsValid)
                return StatusCode(400, ModelState);
            if (string.IsNullOrWhiteSpace(todo.Tarea))
                return StatusCode(400, "No se indico una tarea. Tareas en blanco no son válidas");
            await bd.Database.ExecuteSqlCommandAsync("Todo.UpdTarea @TodoID, @Tarea, @Completada", new[] { new SqlParameter("TodoID", todo.TodoID), new SqlParameter("Tarea", todo.Tarea), new SqlParameter("Completada", todo.Completada) });
            return StatusCode(200, "Actualizado");
        }

        [Route("DelTarea"), HttpDelete, Authorize]
        public async Task<IActionResult> DelTarea([FromBody] Models.Clases.Todo todo)
        {
            if (!ModelState.IsValid)
                return StatusCode(400, ModelState);
            if (todo.TodoID < 1)
                return StatusCode(400, "No se indico una tarea a eliminar. Se debe especificar el ID");
            await bd.Database.ExecuteSqlCommandAsync("Todo.DelTarea @TodoID", new[] { new SqlParameter("TodoID", todo.TodoID) });
            return StatusCode(200, "Insertado");
        }

    }
}
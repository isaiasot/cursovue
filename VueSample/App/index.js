import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import {routes} from './routes/routes'
import ComponenteTituloPagina from "./MisComponentes/TituloPagina"
import ComponenteAlerta from "./MisComponentes/MisAlertas"

// trae el url que se indico en el config.js export {}
import {url} from "../wwwroot/config"

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
Vue.use(Vuetify, {
    theme: {
        primary: '#177EAD' // Sobreescribe el color primario
        ,secondary: '#d32f2f'  // Sobreescribe el color secundario
        ,hacsys: '#177EAD' // Crea el set de color HACSYS
        ,hacsysNegro: "#000000"
    }
})

Vue.use(VueRouter)


window.axios = axios

axios.defaults.baseURL = process.env.NODE_ENV == 'development' ? `${window.location.protocol}//${window.location.host}/api/` : url

/*  =========================================
    AGREGAR COMPONENTE DE TITULO DE PA LAGINA
    ========================================= */
Vue.component("pagina-titulo", ComponenteTituloPagina)
    //  =========================================
/*  =========================================
    AGREGAR MI ALERTAS
    ========================================= */
Vue.component("mi-alerta", ComponenteAlerta)
    //  =========================================
        
const router = new VueRouter({
    routes,
    mode: 'history'
})


new Vue({
    el: '#app',
    template: '<div><router-view></router-view></div>',
    router
})




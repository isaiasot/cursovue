const Reglas = {
    // Para valores requeridos
    requerido: value => !!value || 'Este valor es quererido'
    // Para un minimo de caracteres
    ,minimo: value => value.length < 5 || 'Al menos 5 caracteres'
    // Para validacion en REGEX, solo mayusculas
    ,mayusculas: value => value.match(/[A-Z]/g) || 'Solo mayusculas'
}

export { Reglas }
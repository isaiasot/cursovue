import Inicio from "../Principal/Inicio"
import Tareas from "../Principal/Tareas"
import LaCalcu from "../Principal/Calculadora"
import Slot from "../Principal/Slot"
import Tabla from "../Principal/Tabla"
import MiLista from "../Principal/MiLista"
import Transicion from "../Principal/Transicion"
import Hija from "../Principal/Hija"
import API from "../Principal/API"

/* ================
SECCION PARA LOGIN
================ */


import Login from "../Todo/Login"
import RutaDePaginaTodo from "../Todo/Inicio"
const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login
    }
    ,{
        path: '/Todo',
        name: 'PaginaDeInicio',
        component: RutaDePaginaTodo
    }
]

/* ================
================ */


// const routes = [
//     {
//         path: '/',
//         name: 'Inicio',
//         component: Inicio,
//         children :[{
//             path: 'Hija',
//             name: 'Hija',
//             component: Hija
//         }]
//     },
//     {
//         path: '/Tareas',
//         name: 'Tareas',
//         component: Tareas
//     },
//     {
//         path: '/MiCalcu',
//         name: 'Calcu',
//         component: LaCalcu
//     },
//     {
//         path: '/Slot',
//         name: 'Slot',
//         component: Slot
//     },
//     {
//         path: '/Tabla',
//         name: 'Tabla',
//         component: Tabla
//     },
//     {
//         path: '/MiLista',
//         name: 'MiLista',
//         component: MiLista
//     },
//     {
//         path: '/Transicion',
//         name: 'Transicion',
//         component: Transicion
//     },
//     {
//         path: '/API',
//         name: 'API',
//         component: API
//     }
    
// ]

export { routes }



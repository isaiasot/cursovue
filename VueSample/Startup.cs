﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Vue.Models.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace Vue
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // ********************************************
            // Configura y activa la Base de Datos
            // ********************************************
            var miConfigBD = new Models.DataBaseConfig();
            Configuration.GetSection("BaseDeDatos").Bind(miConfigBD);
            services.AddDbContext<TODOContext>(opciones => opciones.UseSqlServer(miConfigBD.CadenaDeConexion));
            // ********************************************

            // ********************************************
            // Configurar Creenciales desde el CONFIG
            // ********************************************
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opciones =>
                opciones.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters {
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "hacsys.com",
                    ValidAudience = "hacsys.com",
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("System.ArgumentOutOfRangeException"))
                }
            );
            // ********************************************

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // ********************************************
                // Agregar middleware 
                // Que sea AutoRefresh (Hot Module Replacement)
                // ********************************************
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions {
                    HotModuleReplacement = true
                });
                // ********************************************
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            // ********************************************
            // Actrivar la autenticacion
            // ********************************************
            app.UseAuthentication();
            // ********************************************

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                // Cuando cargue la pagina, que cargue sobre la vista INDEX. INDEX es la unica
                routes.MapSpaFallbackRoute(
                    name: "MiPrograma",
                    defaults: new { controller = "Home", action = "Index" }
                );
                // --------------------------------------------
            });
        }
    }
}

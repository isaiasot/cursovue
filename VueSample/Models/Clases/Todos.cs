﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vue.Models.Clases
{
    public class Todo
    {
        public int TodoID { get; set; }
        public string Tarea { get; set; }
        public bool Completada { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vue.Models.Clases
{
    public class TarjetaAcceso
    {
        public string QuienEres { get; set; }
        public string LlaveDeAcceso { get; set; }
    }

    public class Credencial
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}

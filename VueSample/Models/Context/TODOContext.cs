﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Vue.Models.Context
{
    public class TODOContext : DbContext
    {
        public TODOContext() { }
        public TODOContext(DbContextOptions<TODOContext> options) : base(options) { }

        public DbSet<Clases.Todo> Todos { get; set; }


        //public async Task<List<T>> GetRanks<T>(string value1, Nullable<decimal> value2)
        //{
        //    SqlParameter value1Input = new SqlParameter("@Param1", value1 ?? (object)DBNull.Value);
        //    SqlParameter value2Input = new SqlParameter("@Param2", value2 ?? (object)DBNull.Value);
        //    List<T> getRanks = await Query<T>().FromSql("STORED_PROCEDURE @Param1, @Param2", value1Input, value2Input).ToListAsync();
        //    return getRanks;
        //}





        //public static IList<T> SqlQuery<T>(this TODOContext db, string sql, params object[] parameters) where T : class
        //{
        //    using (var db2 = new ContextForQueryType<T>(db.Database.GetDbConnection()))
        //    {
        //        return db2.Query<T>().FromSql(sql, parameters).ToList();
        //    }
        //}
        //private class ContextForQueryType<T> : TODOContext where T : class
        //{
        //    private readonly TODOContext connection;
        //    public ContextForQueryType(TODOContext connection)
        //    {
        //        this.connection = connection;
        //    }
        //    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //    {
        //        // switch on the connection type name to enable support multiple providers
        //        // var name = con.GetType().Name;
        //        //optionsBuilder.UseSqlServer(connection: connection, options => options.EnableRetryOnFailure());
        //        base.OnConfiguring(optionsBuilder);
        //    }
        //    protected override void OnModelCreating(ModelBuilder modelBuilder)
        //    {
        //        modelBuilder.Query<T>();
        //        //base.OnModelCreating(modelBuilder);
        //    }
        //}


    }
}
